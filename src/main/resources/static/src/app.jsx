function ModalWindow(props) {
	return (
		<div className="modal fade" id="infoModal" tabIndex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	      <div className="modal-dialog" role="document">
	        <div className="modal-content rounded-4 shadow">
	          <div className="modal-body p-4">
	            <h5 className="mb-2 text-center">Добавление telegram аккаунта</h5>
	              <ol>
	                <li>Для вас был сгенерирован код {props.code}</li>
	                <li>Откройте приложение телеграм</li>
	                <li>Найдите @DairyBotBot</li>
	                <li>Напишите боту @DairyBotBot сообщение /reg КОД</li>
	                <cite title="Source Title">Пример: /reg ABCD</cite>
	                <li>Вам придет уведомление об успешной привязке</li>
                </ol>
	          </div>
	          <div className="modal-footer flex-nowrap p-0">
	            <button type="button" className="btn btn-lg btn-link fs-6 text-decoration-none col-12 m-0 rounded-0" data-bs-dismiss="modal">Готово</button>
	          </div>
	        </div>
	      </div>
    	</div>
	);
}

function LinkTelegramButton(props) {
	return (
		<div className="col-sm-6">
          <button type="button" className="btn btn-primary" onClick={props.settingsFormObj.linkTelegram}>
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-telegram" viewBox="0 0 16 16">
              <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM8.287 5.906c-.778.324-2.334.994-4.666 2.01-.378.15-.577.298-.595.442-.03.243.275.339.69.47l.175.055c.408.133.958.288 1.243.294.26.006.549-.1.868-.32 2.179-1.471 3.304-2.214 3.374-2.23.05-.012.12-.026.166.016.047.041.042.12.037.141-.03.129-1.227 1.241-1.846 1.817-.193.18-.33.307-.358.336a8.154 8.154 0 0 1-.188.186c-.38.366-.664.64.015 1.088.327.216.589.393.85.571.284.194.568.387.936.629.093.06.183.125.27.187.331.236.63.448.997.414.214-.02.435-.22.547-.82.265-1.417.786-4.486.906-5.751a1.426 1.426 0 0 0-.013-.315.337.337 0 0 0-.114-.217.526.526 0 0 0-.31-.093c-.3.005-.763.166-2.984 1.09z"/>
            </svg>
            Привязать
          </button>
		</div>
	);
}

function TelegramLinkedMessage() {
	return (
		<div className="col-sm-6">
          <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-check-circle text-success" viewBox="0 0 16 16">
            <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
            <path d="M10.97 4.97a.235.235 0 0 0-.02.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-1.071-1.05z"/>
          </svg>
          <span className="text-success">Аккаунт привязан</span>
		</div>
	);
}

function SvgSettings() {
	return (
		<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-tools me-2" viewBox="0 0 16 16">
		  <path d="M1 0 0 1l2.2 3.081a1 1 0 0 0 .815.419h.07a1 1 0 0 1 .708.293l2.675 2.675-2.617 2.654A3.003 3.003 0 0 0 0 13a3 3 0 1 0 5.878-.851l2.654-2.617.968.968-.305.914a1 1 0 0 0 .242 1.023l3.356 3.356a1 1 0 0 0 1.414 0l1.586-1.586a1 1 0 0 0 0-1.414l-3.356-3.356a1 1 0 0 0-1.023-.242L10.5 9.5l-.96-.96 2.68-2.643A3.005 3.005 0 0 0 16 3c0-.269-.035-.53-.102-.777l-2.14 2.141L12 4l-.364-1.757L13.777.102a3 3 0 0 0-3.675 3.68L7.462 6.46 4.793 3.793a1 1 0 0 1-.293-.707v-.071a1 1 0 0 0-.419-.814L1 0zm9.646 10.646a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708zM3 11l.471.242.529.026.287.445.445.287.026.529L5 13l-.242.471-.026.529-.445.287-.287.445-.529.026L3 15l-.471-.242L2 14.732l-.287-.445L1.268 14l-.026-.529L1 13l.242-.471.026-.529.445-.287.287-.445.529-.026L3 11z"/>
		</svg>
	);
}

function SvgCalendar() {
	return (
		<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-calendar-check me-2" viewBox="0 0 16 16">
  			<path d="M10.854 7.146a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 1 1 .708-.708L7.5 9.793l2.646-2.647a.5.5 0 0 1 .708 0z"/>
  			<path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4H1z"/>
		</svg>
	);
}

class MenuItem extends React.Component {
	constructor(props) {
    	super(props);
    	this.handleClick = this.handleClick.bind(this);
  	}

  	handleClick() {
  		this.props.handler(this.props.menuName);
  	}

	render() {
    	return (
			<a href="#" onClick={this.handleClick} className={this.props.linkType}>
				{this.props.menuImage}
	  			{this.props.menuName}
		    </a>
		);
  	}
}


class Navigation extends React.Component {
	constructor(props) {
    	super(props);
    	this.state = {
    		activeMenuItem: "Ежедневник"
    	};
    	this.handler = this.handler.bind(this);
    	this.handleLogout = this.handleLogout.bind(this);
  	}

  	handler(menuName) {
	    this.setState({activeMenuItem: menuName});
	    this.props.handler(menuName);
  	}

  	handleLogout() {
  		logout();
  	}

	render() {
    	return (
			<div className="d-flex flex-column flex-shrink-0 p-3 bg-light navMenuDiv">
				<a href="/" className="d-flex align-items-center mb-3 mb-md-0 me-md-auto link-dark text-decoration-none">
		          <img src="./img/logo.svg" width="40" height="32" />
		          <span className="fs-4 ps-2">SDiary</span>
		        </a>
		     	<hr />
		     	<ul className="nav nav-pills flex-column mb-auto">
		     		<li className="nav-item">
		     			<MenuItem menuName="Настройки" linkType={"Настройки" === this.state.activeMenuItem ? "nav-link active" : "nav-link link-dark"} menuImage={<SvgSettings />} handler = {this.handler} />
		     		</li>
		     		<li className="nav-item">
			            <MenuItem menuName="Ежедневник" linkType={"Ежедневник" === this.state.activeMenuItem ? "nav-link active" : "nav-link link-dark"} menuImage={<SvgCalendar />} handler = {this.handler} />
		          </li>
		     	</ul>
		     	<hr />
         		<div>
	            	<button type="button" className="btn btn-primary btn-sm me-1" title="Выход" onClick={this.handleLogout}>
	                	<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-box-arrow-left" viewBox="0 0 16 16">
	                    	<path fillRule="evenodd" d="M6 12.5a.5.5 0 0 0 .5.5h8a.5.5 0 0 0 .5-.5v-9a.5.5 0 0 0-.5-.5h-8a.5.5 0 0 0-.5.5v2a.5.5 0 0 1-1 0v-2A1.5 1.5 0 0 1 6.5 2h8A1.5 1.5 0 0 1 16 3.5v9a1.5 1.5 0 0 1-1.5 1.5h-8A1.5 1.5 0 0 1 5 12.5v-2a.5.5 0 0 1 1 0v2z"/>
	                    	<path fillRule="evenodd" d="M.146 8.354a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L1.707 7.5H10.5a.5.5 0 0 1 0 1H1.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3z"/>
	                  	</svg>
	              	</button>
	              	<strong>{this.props.activeUserName}</strong>
         		</div>
		    </div>
		);
  	}
}


function NavigationDivider() {
	return (
		<div className="b-example-divider"></div>
	);
}

class PrimaryContentHeader extends React.Component {
	constructor(props) {
    	super(props);
  	}

	render() {
    	return (
    		<div className="mt-3">
      			<span className="fs-4">{this.props.activeMenuItem}</span>
      		</div>
    	);
  	}
}

function AlertDiv() {
	return (
		<div className="settings mt-3">
          <div id="alertDiv"></div>
        </div>
	);
}

class SettingsForm extends React.Component {
	constructor(props) {
    	super(props);
    	this.state = {
    		lastName: "",
    		firstName: "",
    		secondName: "",
    		telegram: "",
    		code: ""
    	};
    	this.linkTelegram = this.linkTelegram.bind(this);
  	}

  	linkTelegram() {
  		let userInfo = getUserInfo();
    	if (userInfo === null) {
        return;
    	}

    	if (userInfo.telegramChatId != null) {
        showAlert("Аккаунт уже привязан к telegram", "success");
        this.setState({telegram: <TelegramLinkedMessage />});
        return;
    	}

    	let telegramCode = generateTelegramCode(userInfo.userId);
    	if (telegramCode === null) {
        return;
    	}

  		this.setState({code: telegramCode});
  		let myModal = new bootstrap.Modal(document.getElementById('infoModal'), {
            keyboard: false
        })
      myModal.show();
  	}

  	componentDidMount() {
  		var settingsFormObj = this;
  		
  		let userInfo = getUserInfo();
    	if (userInfo != null) {
    		let telegram = null;
    		if (userInfo.telegramChatId === null) {
        		telegram = <LinkTelegramButton settingsFormObj={this} />;
        	}
        	else {
        		telegram = <TelegramLinkedMessage />;
        	}

    		this.setState({
    			lastName: userInfo.lastName,
    			firstName: userInfo.firstName,
    			secondName: userInfo.secondName,
    			telegram: telegram
    		});
    	}

    	$( "#settingsForm" ).validate( {
	        rules: {
	            lastName: "required",
	            firstName: "required"      
	        },
	        messages: {
	            lastName: "Введите фамилию",
	            firstName: "Введите имя"
	        },
	        errorElement: "em",
	        errorPlacement: function ( error, element ) {
	            element.addClass( "is-invalid" );
	            error.addClass( "invalid-feedback" );
	            error.insertAfter( element );
	        },
	        highlight: function ( element, errorClass, validClass ) {
	            $( element ).addClass( "is-invalid" ).removeClass( "is-valid" );
	        },
	        unhighlight: function (element, errorClass, validClass) {
	            $( element ).addClass( "is-valid" ).removeClass( "is-invalid" );
	        },
	        submitHandler: function(form) {
	            let userInfo = getUserInfo();
	            if (userInfo === null) {
	                return;
	            }
	            let user = $(form).serializeJSON();
	            emptyToNull(user);
	            let mergedUserInfo = Object.assign(userInfo, user);
	            updateUser(mergedUserInfo);
	            settingsFormObj.props.handler($(form).find('input[name="lastName"]').val() + " " + $(form).find('input[name="firstName"]').val());
	        }
    	} );


    	$( "#passwordChangeForm" ).validate( {
	        rules: {
	            password: "required",
	            passwordConfirm: {
	                required: true,
	                equalTo: "#password"
	            }      
	        },
	        messages: {
	            password: "Введите пароль",
	            passwordConfirm: {
	                required: "Подтвердите ввод пароля",
	                equalTo: "Пароли должны совпадать"
	            }
	        },
	        errorElement: "em",
	        errorPlacement: function ( error, element ) {
	            element.addClass( "is-invalid" );
	            error.addClass( "invalid-feedback" );
	            error.insertAfter( element );
	        },
	        highlight: function ( element, errorClass, validClass ) {
	            $( element ).addClass( "is-invalid" ).removeClass( "is-valid" );
	        },
	        unhighlight: function (element, errorClass, validClass) {
	            $( element ).addClass( "is-valid" ).removeClass( "is-invalid" );
	        },
	        submitHandler: function(form) {
	            let userInfo = getUserInfo();
	            if (userInfo === null) {
	                return;
	            }
	            let mergedUserInfo = Object.assign(userInfo, $(form).serializeJSON())
	            delete mergedUserInfo.passwordConfirm;
	            updateUser(mergedUserInfo);
	        }
    	} );
  	}

	render() {
		return (
			<div className="row">
			<ModalWindow code={this.state.code} />
				<div className="settings mt-3">
					<form id="settingsForm">
						<div className="row mb-2">
			                <label className="col-sm-4 col-form-label" htmlFor="lastName">Фамилия</label>
			                <div className="col-sm-6">
			                	<input type="text" className="form-control" name="lastName" defaultValue={this.state.lastName} />
			                </div>
			            </div>

			            <div className="row mb-2">
			                <label className="col-sm-4 col-form-label" htmlFor="firstName">Имя</label>
			                <div className="col-sm-6">
			                  <input type="text" className="form-control" name="firstName" defaultValue={this.state.firstName} />
			                </div>
			            </div>

			            <div className="row mb-2">
			                <label className="col-sm-4 col-form-label" htmlFor="secondName">Отчество</label>
			                <div className="col-sm-6">
			                  <input type="text" className="form-control" name="secondName" defaultValue={this.state.secondName} />
			                </div>
			            </div>

			            <div className="row mb-2">
			                <label className="col-sm-4 col-form-label">Telegram аккаунт</label>
			                {this.state.telegram} 
			            </div>

			            <div className="text-center">
	                		<button type="submit" className="btn btn-primary">Изменить данные</button>
	              		</div>
					</form>
				</div>

				<div className="settings mt-3">
		            <form id="passwordChangeForm">
		              <div className="row mb-2">
		                <label className="col-sm-4 col-form-label" htmlFor="password">Новый пароль</label>
		                <div className="col-sm-6">
		                  <input type="password" className="form-control" name="password" id="password" />
		                </div>
		              </div>

		              <div className="row mb-2">
		                <label className="col-sm-4 col-form-label" htmlFor="passwordConfirm">Подтверждение пароля</label>
		                <div className="col-sm-6">
		                  <input type="password" className="form-control" name="passwordConfirm" />
		                </div>
		              </div>

		              <div className="text-center">
		                <button type="submit" className="btn btn-primary">Изменить пароль</button>
		              </div>
		            </form>
		         </div>
			</div>
		);
	}
}

class DatePicker extends React.Component {
	constructor(props) {
    	super(props);
    	this.state = {
    		currentDate: new Date().toDateInputValue()
    	};
    	this.handleChangeDateClick = this.handleChangeDateClick.bind(this);
    	this.handleChange = this.handleChange.bind(this);
  	}

  	handleChange() {
  		this.setState({currentDate: event.target.value});
  		this.props.handler(event.target.value);
  	}
  	
  	handleChangeDateClick(days) {
  		let date = new Date(this.state.currentDate);
    	date.setDate(date.getDate() + days);
    	this.setState({currentDate: date.toDateInputValue()});
  		this.props.handler(date.toDateInputValue());
  	}

  	render() {
  		return (
  			<div className="date-picker input-group mt-3">
	          <button className="btn btn-primary" type="button" onClick={() => this.handleChangeDateClick(-1)}>
	            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-arrow-left" viewBox="0 0 16 16">
	              <path fillRule="evenodd" d="M15 8a.5.5 0 0 0-.5-.5H2.707l3.147-3.146a.5.5 0 1 0-.708-.708l-4 4a.5.5 0 0 0 0 .708l4 4a.5.5 0 0 0 .708-.708L2.707 8.5H14.5A.5.5 0 0 0 15 8z"/>
	            </svg>
	          </button>
	          <input type="date" className="form-control" value={this.state.currentDate} onChange={this.handleChange} />
	          <button className="btn btn-primary" type="button" onClick={() => this.handleChangeDateClick(1)}>
	            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-arrow-right" viewBox="0 0 16 16">
	              <path fillRule="evenodd" d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z"/>
	            </svg>
	          </button>
        	</div>
  		);
  	}
}

class EventEntityRow extends React.Component {
	constructor(props) {
    	super(props);
    	this.handleFinishEvent = this.handleFinishEvent.bind(this);
    	this.handleCancelEvent = this.handleCancelEvent.bind(this);
    	this.handleDeleteEvent = this.handleDeleteEvent.bind(this);
  	}

  	handleFinishEvent(eventId) {
  		let isSuccess = finishEvent(eventId);
  		if (isSuccess === true) {
  			this.props.handler();
  		}
  	}

  	handleCancelEvent(eventId) {
  		let isSuccess = cancelEvent(eventId);
  		if (isSuccess === true) {
  			this.props.handler();
  		}
  	}

  	handleDeleteEvent(eventId) {
  		let isSuccess = deleteEvent(eventId);
  		if (isSuccess === true) {
  			this.props.handler();
  		}
  	}

  	render() {
  		return (
  			<tr>
		        <td>{this.props.num}</td>
		        <td>{this.props.name}</td>
		        <td>{this.props.description}</td>
		        <td>{this.props.time}</td>
		        <td>{this.props.state}</td>
		        <td>{this.props.priority}</td>
		        <td>
		        	<div className="d-flex flex-row">
				        <button type="button" className="btn btn-success btn-sm me-2" title="Завершить" onClick={() => this.handleFinishEvent(this.props.eventId)}>
				            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-calendar-check" viewBox="0 0 16 16">
							  <path d="M10.854 7.146a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 1 1 .708-.708L7.5 9.793l2.646-2.647a.5.5 0 0 1 .708 0z"/>
							  <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4H1z"/>
							</svg>
				        </button>

				        <button type="button" className="btn btn-secondary btn-sm me-2" title="Отменить" onClick={() => this.handleCancelEvent(this.props.eventId)}>
				            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-x-lg" viewBox="0 0 16 16">
  							  <path fillRule="evenodd" d="M13.854 2.146a.5.5 0 0 1 0 .708l-11 11a.5.5 0 0 1-.708-.708l11-11a.5.5 0 0 1 .708 0Z"/>
							  <path fillRule="evenodd" d="M2.146 2.146a.5.5 0 0 0 0 .708l11 11a.5.5 0 0 0 .708-.708l-11-11a.5.5 0 0 0-.708 0Z"/>
							</svg>
				        </button>
				        
				        <button type="button" className="btn btn-danger btn-sm" title="Удалить" onClick={() => this.handleDeleteEvent(this.props.eventId)}>
				            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-trash" viewBox="0 0 16 16">
							  <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
							  <path fillRule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
							</svg>
				        </button>
			        </div>
		        </td>
    		</tr>
  		);
  	}
}

class AddEventForm extends React.Component {
	constructor(props) {
    	super(props);
  	}

  	render() {
  		return (
  			<tr id="addEvent" className="d-none">
	          <td></td>
	          <td><input type="text" className="form-control form-control-sm" name="name" id="eventFormName" /></td>
	          <td><textarea className="form-control form-control-sm" name="description" id="eventFormDescription"></textarea></td>
	          <td><input type="time" className="form-control form-control-sm" name="time" id="eventFormTime" /></td>
	          <td>
	            <select className="form-select form-select-sm" defaultValue="TODO" name="state">
	              <option value="TODO">{codeToValue("TODO")}</option>
	              <option value="CANCELED">{codeToValue("CANCELED")}</option>
	              <option value="DONE">{codeToValue("DONE")}</option>
	            </select>
	          </td>
	          <td>
	            <select className="form-select form-select-sm" defaultValue="HIGH" name="priority">
	              <option value="HIGH">{codeToValue("HIGH")}</option>
	              <option value="MEDIUM">{codeToValue("MEDIUM")}</option>
	              <option value="LOW">{codeToValue("LOW")}</option>
	            </select>
	          </td>
	          <td>
	            <button type="submit" title="Сохранить" className="btn btn-primary btn-sm">
	              <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-save" viewBox="0 0 16 16">
	                <path d="M2 1a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H9.5a1 1 0 0 0-1 1v7.293l2.646-2.647a.5.5 0 0 1 .708.708l-3.5 3.5a.5.5 0 0 1-.708 0l-3.5-3.5a.5.5 0 1 1 .708-.708L7.5 9.293V2a2 2 0 0 1 2-2H14a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2h2.5a.5.5 0 0 1 0 1H2z"/>
	              </svg>
	            </button>
	          </td>
    		</tr>
  		);
  	}
}

class EventsTable extends React.Component {
	constructor(props) {
    	super(props);
    	this.handler = this.handler.bind(this);
  	}

  	componentDidMount() {
  		var eventsTableObj = this;
  		$("#showAddEventFormButton").click(function(){
        	showAddEventForm();
    	});

    	$("#hideAddEventFormButton").click(function(){
        	hideAddEventForm();
        	clearAddEventForm();
    	});

    	$( "#addEventForm" ).validate( {
	        rules: {
	            name: "required",
	            state: "required",
	            priority: "required"
	        },
	        messages: {
	            name: "Введите имя",
	            state: "Введите статус",
	            priority: "Введите приоритет"
	        },
	        errorElement: "em",
	        errorPlacement: function ( error, element ) {
	            element.addClass( "is-invalid" );
	            error.addClass( "invalid-feedback" );
	            error.insertAfter( element );
	        },
	        highlight: function ( element, errorClass, validClass ) {
	            $( element ).addClass( "is-invalid" ).removeClass( "is-valid" );
	        },
	        unhighlight: function (element, errorClass, validClass) {
	            $( element ).addClass( "is-valid" ).removeClass( "is-invalid" );
	        },
	        submitHandler: function(form) {
	        	let userId = null;
			    let userInfo = getUserInfo();
			    if (userInfo != null) {
			        userId = userInfo.userId;
			    }

			    if (userId != null) {
			    	let event = $(form).serializeJSON();
	            	event.id = null;
	            	event.userId = userId;
	            	event.date = eventsTableObj.props.currentDate;
	            	emptyToNull(event);
	            	
	            	let status = createEvent(event);
	            	if (status === "success") {
	            		hideAddEventForm();
	            		clearAddEventForm();
			  			eventsTableObj.forceUpdate();
			  			showAlert("Событие успешно добавлено", "success");
			  		}
			  		else {
			  			showAlert("Ошибка при создании события: " + errorThrown, "danger");
			  		}
			    }
	        }
    	} );
  	}

  	handler() {
	    this.forceUpdate();
  	}

  	render() {
  		let events = getUserEventsByDate(Cookies.get("userId"), this.props.currentDate);
  		const eventRows = [];
  		for (i in events) {
  			eventRows.push(<EventEntityRow handler = {this.handler} key={events[i].id} num={parseInt(i) + 1} eventId={events[i].id} name={events[i].name} description={events[i].description} time={events[i].time === null ? null : events[i].time.slice(0, -3)} state={codeToValue(events[i].state)} priority={codeToValue(events[i].priority)} />);
  		}
  		return (
  			<div className="cal-table mt-3">
	          <form id="addEventForm">
	            <table className="table">
	              <thead>
	                <tr>
	                  <th scope="col">Номер</th>
	                  <th scope="col">Название</th>
	                  <th scope="col">Описание</th>
	                  <th scope="col">Время</th>
	                  <th scope="col">Статус</th>
	                  <th scope="col">Приоритет</th>
	                  <th scope="col"></th>
	                </tr>
	              </thead>
	              <tbody>
	              	{eventRows}
	              	<AddEventForm />
	              </tbody>
	            </table>
	          </form>
	          <div className="text-center">
	            <button type="button" className="btn btn-primary me-2" id="showAddEventFormButton">Добавить событие</button>
	            <button type="button" className="btn btn-secondary d-none" id="hideAddEventFormButton">Отмена</button>
	          </div>
        	</div>
  		);
  	}
}

class Calendar extends React.Component {
	constructor(props) {
    	super(props);
    	this.state = {
    		currentDate: new Date().toDateInputValue()
    	};
    	this.handler = this.handler.bind(this);
  	}

  	handler(date) {
	    this.setState({currentDate: date});
  	}

  	render() {
  		return (
  			<div>
  				<DatePicker handler = {this.handler} />
  				<EventsTable currentDate={this.state.currentDate} />
  			</div>
  		);
  	}
}

class PrimaryContent extends React.Component {
	constructor(props) {
    	super(props);
    	this.changeCurrentUserName = this.changeCurrentUserName.bind(this);
  	}

  	changeCurrentUserName(name) {
  		this.props.handler(name);
  	}

  	render() {
  		let content;
  		if (this.props.activeMenuItem === "Ежедневник") {
      		content = <Calendar />;
    	}
    	else {
      		content = <SettingsForm handler={this.changeCurrentUserName} />;
   		}
  		return (
			<div className="container-fluid bg-light scrollarea">
				<PrimaryContentHeader activeMenuItem={this.props.activeMenuItem} />
				<AlertDiv />
				{content}
			</div>
		);
  	}
}

class App extends React.Component {
	constructor(props) {
    	super(props);
    	this.state = {
    		activeMenuItem: "Ежедневник",
    		activeUserName: ""
    	};
    	this.handler = this.handler.bind(this);
    	this.changeCurrentUserName = this.changeCurrentUserName.bind(this);
  	}

  	componentDidMount() {
  		let userId = null;
    	let userInfo = getUserInfo();
    	if (userInfo != null) {
        	userId = userInfo.userId;
        	this.setState({activeUserName: userInfo.lastName + " " + userInfo.firstName});
    	}
    	Cookies.set("userId", userId);
  	}

  	handler(menuName) {
	    this.setState({activeMenuItem: menuName});
  	}

  	changeCurrentUserName(name) {
  		this.setState({activeUserName: name});
  	}

  	render() {
  		return (
			<main>
				<Navigation handler = {this.handler} activeUserName={this.state.activeUserName} />
				<NavigationDivider />
				<PrimaryContent activeMenuItem={this.state.activeMenuItem} handler={this.changeCurrentUserName} />
			</main>
		);
  	}
}

ReactDOM.render(<App />, document.getElementById('root'));