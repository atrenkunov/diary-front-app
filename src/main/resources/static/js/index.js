$(function() {
	$("#regButton").click(function(){
        window.location.href = "reg.html";
    });

    $( "#loginForm" ).validate( {
        rules: {
            login: "required",
            password: "required"
        },
        messages: {
            login: "Введите логин",
            password: "Введите пароль"
        },
        errorElement: "em",
        errorPlacement: function ( error, element ) {
            element.addClass( "is-invalid" );
            error.addClass( "invalid-feedback" );
            error.addClass( "text-start" );
            error.insertAfter( element );
        },
        highlight: function ( element, errorClass, validClass ) {
            $( element ).addClass( "is-invalid" ).removeClass( "is-valid" );
        },
        unhighlight: function (element, errorClass, validClass) {
            $( element ).addClass( "is-valid" ).removeClass( "is-invalid" );
        },
        submitHandler: function(form) {
            authorizeUser($(form).serializeJSON());
        }
    } );
});