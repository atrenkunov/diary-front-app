$(function() {
	$("#loginButton").click(function(){
        window.location.href = "index.html";
    });

    $( "#regForm" ).validate( {
        rules: {
            lastName: "required",
            firstName: "required",
            birthDate: "required",
            login: "required",
            password: "required",
            passwordConfirm: {
                required: true,
                equalTo: "#password"
            }
        },
        messages: {
            lastName: "Введите фамилию",
            firstName: "Введите имя",
            birthDate: "Выберите дату рождения",
            login: "Введите логин",
            password: "Введите пароль",
            passwordConfirm: {
                required: "Подтвердите ввод пароля",
                equalTo: "Пароли должны совпадать"
            }
        },
        errorElement: "em",
        errorPlacement: function ( error, element ) {
            element.addClass( "is-invalid" );
            error.addClass( "invalid-feedback" );
            error.insertAfter( element );
        },
        highlight: function ( element, errorClass, validClass ) {
            $( element ).addClass( "is-invalid" ).removeClass( "is-valid" );
        },
        unhighlight: function (element, errorClass, validClass) {
            $( element ).addClass( "is-valid" ).removeClass( "is-invalid" );
        },
        submitHandler: function(form) {
            let user = $(form).serializeJSON();
            user.userId = null;
            user.telegramChatId = null;
            user.roles = null;
            delete user.passwordConfirm;
            emptyToNull(user);
            registerUser(user);
        }
    } );       
});



