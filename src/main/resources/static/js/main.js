function isIE() {
    const ua = window.navigator.userAgent; 
    const msie = ua.indexOf('MSIE '); 
    const trident = ua.indexOf('Trident/');
    
    return (msie > 0 || trident > 0);
}

function ieError() {
    if(isIE()) {
       window.location.href = "oldbrowser.html";
    }
}

function emptyToNull(obj) {
    for (const [key, value] of Object.entries(obj)) {
        if (value != null && value.length === 0) {
            obj[key] = null;
        } 
    }
}

function showAlert(message, type) {
    $( "#alertDiv" ).append( $( '<div class="alert alert-' + type + ' alert-dismissible" role="alert">' + message + '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div>' ) );
}

function codeToValue(code) {
    const codeMap = new Map();
    codeMap.set("TODO", "Запланировано");
    codeMap.set("CANCELED", "Отменено");
    codeMap.set("DONE", "Выполнено");
    codeMap.set("HIGH", "Высокий");
    codeMap.set("MEDIUM", "Средний");
    codeMap.set("LOW", "Низкий");
    return codeMap.get(code);
}

var path = "";

function registerUser(user) {
    $.ajax({
        async: false,
        type: "POST",
        url: path + "diary/auth/user",
        data: JSON.stringify(user),
        contentType: "application/json; charset=UTF-8",
        success: function(data, textStatus, jqXHR) {
            showAlert('Регистрация успешно завершена. <a href="index.html" class="alert-link">Войти</a>', "success");
        },
        error: function(jqXHR, textStatus, errorThrown) {
            showAlert("Ошибка при регистрации: " + errorThrown, "danger");
        },
        dataType: "json"
    });
}

function getUserInfo() {
    let userData = null;
    $.ajax({
        async: false,
        type: "GET",
        url: path + "diary/auth/info",
        success: function(data, textStatus, jqXHR) {
            userData = data;
        },
        error: function(jqXHR, textStatus, errorThrown) {
            showAlert("Ошибка при получении данных с сервера: " + errorThrown, "danger");
        },
        dataType: "json"
    });
    return userData;
}

function updateUser(user) {
    $.ajax({
        async: false,
        type: "PUT",
        url: path + "diary/auth/user",
        data: JSON.stringify(user),
        contentType: "application/json; charset=UTF-8",
        success: function(data, textStatus, jqXHR) {
            showAlert("Данные успешно изменены", "success");
        },
        error: function(jqXHR, textStatus, errorThrown) {
            showAlert("Произошла ошибка: " + errorThrown, "danger");
        },
        dataType: "json"
    });
}

function authorizeUser(authorizeDTO) {
    $.ajax({
        async: false,
        type: "POST",
        url: path + "diary/auth/authorize",
        data: JSON.stringify(authorizeDTO),
        contentType: "application/json; charset=UTF-8",
        success: function(data, textStatus, jqXHR) {
            window.location.href = "app.html";
        },
        error: function(jqXHR, textStatus, errorThrown) {
            if (jqXHR.status == 500) {
                showAlert("Неверный логин и пароль", "danger");
            }
            else {
                showAlert("Произошла ошибка при авторизации: " + errorThrown, "danger");
            }
        }
    });
}

function generateTelegramCode(userId) {
    let telegramCode = null;
    $.ajax({
        async: false,
        type: "POST",
        url: path + "diary/auth/telegram/generateCode?userId=" + userId,
        success: function(data, textStatus, jqXHR) {
            telegramCode = data;
        },
        error: function(jqXHR, textStatus, errorThrown) {
            showAlert("Ошибка при получении данных с сервера: " + errorThrown, "danger");
        },
        dataType: "text"
    });
    return telegramCode;
}

function getUserEventsByDate(userId, date) {
    let events = null;
    $.ajax({
        async: false,
        type: "GET",
        url: path + "diary/event/all?userId=" + userId + "&date=" + date,
        success: function(data, textStatus, jqXHR) {
            events = data;
        },
        error: function(jqXHR, textStatus, errorThrown) {
            showAlert("Ошибка при получении данных с сервера: " + errorThrown, "danger");
        },
        dataType: "json"
    });
    return events;
}

function finishEvent(eventId) {
    let isSuccess = null;
    $.ajax({
        async: false,
        type: "PUT",
        url: path + "diary/event/" + eventId + "/finish",
        success: function(data, textStatus, jqXHR) {
            showAlert("Событие успешно завершено", "success");
            isSuccess = true;
        },
        error: function(jqXHR, textStatus, errorThrown) {
            showAlert("Ошибка при завершении события: " + errorThrown, "danger");
            isSuccess = false;
        },
        dataType: "json"
    });
    return isSuccess;
}

function cancelEvent(eventId) {
    let isSuccess = null;
    $.ajax({
        async: false,
        type: "PUT",
        url: path + "diary/event/" + eventId + "/cancel",
        success: function(data, textStatus, jqXHR) {
            showAlert("Событие успешно отменено", "success");
            isSuccess = true;
        },
        error: function(jqXHR, textStatus, errorThrown) {
            showAlert("Ошибка при отмене события: " + errorThrown, "danger");
            isSuccess = false;
        },
        dataType: "json"
    });
    return isSuccess;
}

function deleteEvent(eventId) {
    let isSuccess = null;
    $.ajax({
        async: false,
        type: "DELETE",
        url: path + "diary/event/" + eventId,
        success: function(data, textStatus, jqXHR) {
            showAlert("Событие успешно удалено", "success");
            isSuccess = true;
        },
        error: function(jqXHR, textStatus, errorThrown) {
            showAlert("Ошибка при удалении события: " + errorThrown, "danger");
            isSuccess = false;
        },
        dataType: "json"
    });
    return isSuccess;
}

function createEvent(event) {
    let status = null;
    $.ajax({
        async: false,
        type: "POST",
        url: path + "diary/event",
        data: JSON.stringify(event),
        contentType: "application/json; charset=UTF-8",
        success: function(data, textStatus, jqXHR) {
            status = "success";
        },
        error: function(jqXHR, textStatus, errorThrown) {
            status = errorThrown;
        },
        dataType: "json"
    });
    return status;
}

function logout() {
    $.ajax({
        async: false,
        type: "GET",
        url: path + "diary/logout",
        success: function(data, textStatus, jqXHR) {
            window.location.href = "index.html";
        },
        error: function(jqXHR, textStatus, errorThrown) {
            showAlert("Произошла ошибка: " + errorThrown, "danger");
        }
    });
}


