function showAddEventForm() {
    $("#addEvent").removeClass("d-none");
    $("#hideAddEventFormButton").removeClass("d-none");
}

function hideAddEventForm() {
    $("#addEvent").addClass("d-none");
    $("#hideAddEventFormButton").addClass("d-none");
}

function clearAddEventForm() {
    $("#eventFormName").val("");
    $("#eventFormDescription").val("");
    $("#eventFormTime").val("");

    $("#eventFormName").removeClass( "is-valid" ).removeClass( "is-invalid" );
    $("#eventFormDescription").removeClass( "is-valid" ).removeClass( "is-invalid" );
    $("#eventFormTime").removeClass( "is-valid" ).removeClass( "is-invalid" );
}

Date.prototype.toDateInputValue = (function() {
    var local = new Date(this);
    local.setMinutes(this.getMinutes() - this.getTimezoneOffset());
    return local.toJSON().slice(0,10);
});

