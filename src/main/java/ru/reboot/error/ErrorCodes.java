package ru.reboot.error;

public enum ErrorCodes {
    EVENT_NOT_FOUND,
    UNAUTHORISED,
    CREATE_EVENT_ERROR,
    REGISTER_USER_ERROR,
    UPDATE_USER_ERROR,
    LOGOUT_ERROR,
    GENERATE_CODE_ERROR

}
