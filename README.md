# Сервис diary-front-app

Фронтовое приложение SDiary

Ответственный за сервис: Ринат Нагуманов

### Реализовать rest-контроллер /diary/**/*, который обеспечивает работу с пользователями

План работ:

Игорь Задубинин
- HTML + JS (ресты)

Александр Жуков
- PUT /diary/event/{eventId}/finish
- PUT /diary/event/{eventId}/cancel
- POST /diary/auth/telegram/generateCode?userId={userId}

Илья Семенов
- POST /diary/auth/authorize
- GET /diary/auth/info

Денис Вузов
- POST /diary/auth/user
- PUT /diary/auth/user

Ринат Нагуманов
- GET /diary/event/all?userId={userId}&date={date}
- DELETE /diary/event/{eventId}
- POST /diary/event

Критерии приемки:
- методы сервиса должны быть протестированы